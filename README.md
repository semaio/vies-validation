# VIES Validation

A simple library to validate European VAT ID numbers against the VIES webservice provided by the European Union.

More information: [http://ec.europa.eu/taxation_customs/vies/](http://ec.europa.eu/taxation_customs/vies/)
