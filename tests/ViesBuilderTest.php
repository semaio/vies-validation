<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Tests;

use PHPUnit\Framework\TestCase;
use Semaio\ViesValidation\Client\HeartbeatClientInterface;
use Semaio\ViesValidation\Client\ViesClientInterface;
use Semaio\ViesValidation\Validation\SyntaxValidator;
use Semaio\ViesValidation\ViesBuilder;
use Semaio\ViesValidation\ViesInterface;

/**
 * Class ViesBuilderTest
 *
 * @package Semaio\ViesValidation\Tests
 */
class ViesBuilderTest extends TestCase
{
    /**
     * @test
     */
    public function build()
    {
        $viesBuilder = new ViesBuilder();
        $viesBuilder->setSoapClient(new SoapClientMock('http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl'));
        $viesBuilder->setSoapOptions([]);
        $viesBuilder->setWsdl('http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl');
        $viesBuilder->setValidator(new SyntaxValidator());

        $vies = $viesBuilder->build();
        $this->assertInstanceOf(ViesInterface::class, $vies);
        $this->assertInstanceOf(ViesClientInterface::class, $vies->getViesClient());
        $this->assertInstanceOf(HeartbeatClientInterface::class, $vies->getHeartbeatClient());
    }

    /**
     * @test
     */
    public function getWsdl()
    {
        $viesBuilder = new ViesBuilder();

        $this->assertEquals(
            'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl',
            $viesBuilder->getWsdl()
        );
    }

    /**
     * @test
     */
    public function getSoapOptions()
    {
        $soapOptions = ['abc' => 'def'];

        $viesBuilder = new ViesBuilder();
        $viesBuilder->setSoapOptions($soapOptions);

        $this->assertEquals(
            $soapOptions,
            $viesBuilder->getSoapOptions()
        );
    }
}
