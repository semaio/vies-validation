<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Tests\Validation;

use PHPUnit\Framework\TestCase;
use Semaio\ViesValidation\Validation\SyntaxValidator;

/**
 * Class SyntaxValidationTest
 *
 * @package Semaio\ViesValidation\Tests\Validation
 */
class SyntaxValidationTest extends TestCase
{
    /**
     * @test
     * @dataProvider vatNumberProvider
     */
    public function testValidator(string $countryCode, string $vatNumber, bool $expectation)
    {
        $this->assertSame($expectation, (new SyntaxValidator())->validate($countryCode, $vatNumber));
    }

    public function vatNumberProvider()
    {
        return [
            // Random
            ['XY', '123456789', false],
            // Austria
            ['AT', 'U10223006', true],
            ['AT', 'U1022300', false],
            ['AT', 'A10223006', false],
            // Belgium
            ['BE', '776091951', false],
            ['BE', '07760919', false],
            ['BE', '0417710407', true],
            ['BE', '0627515170', true],
            // Bulgaria
            ['BG', '301004503', true],
            ['BG', '10100450', false],
            ['BG', '8311046307', true],
            // Cyprus
            ['CY', '00532445O', true],
            ['CY', '005324451', false],
            ['CY', '0053244511', false],
            // Czech Republic
            ['CZ', '46505334', true],
            ['CZ', '7103192745', true],
            ['CZ', '640903926', true],
            ['CZ', '395601439', true],
            ['CZ', '630903928', true],
            ['CZ', '27082440', true],
            ['CZ', '4650533', false],
            // Germany
            ['DE', '123456789', true],
            ['DE', '12345678', false],
            ['DE', '1234567890', false],
            // Denmark
            ['DK', '88146328', true],
            ['DK', '1234567', false],
            // Estonia
            ['EE', '100207415', true],
            ['EE', '1002074', false],
            ['EE', 'A12345678', false],
            // Greece
            ['EL', '040127797', true],
            ['EL', '1234567', false],
            // Spain
            ['ES', 'A0011012B', true],
            ['ES', 'A78304516', true],
            ['ES', 'X5910266W', true],
            ['ES', '12345678', false],
            ['ES', 'K001A012B', false],
            // Finland
            ['FI', '09853608', true],
            ['FI', '1234567', false],
            // France
            ['FR', '00300076965', true],
            ['FR', 'K7399859412', true],
            ['FR', '4Z123456782', true],
            ['FR', '0030007696A', false],
            ['FR', '1234567890', false],
            // United Kingdom
            ['GB', '434031494', true],
            ['GB', 'GD001', true],
            ['GB', 'HA500', true],
            ['GB', '12345', false],
            ['GB', '12345678', false],
            // Croatia
            ['HR', '38192148118', true],
            ['HR', '3819214811', false],
            ['HR', '1234567890A', false],
            ['HR', 'AA123456789', false],
            // Hungary
            ['HU', '21376414', true],
            ['HU', '10597190', true],
            ['HU', '2137641', false],
            ['HU', '1234567A', false],
            // Ireland
            ['IE', '3628739L', true],
            ['IE', '5343381W', true],
            ['IE', '6433435OA', true],
            ['IE', '8Z49389F', false],
            ['IE', '1234567', false],
            // Italy
            ['IT', '00000010215', true],
            ['IT', '1234567890', false],
            ['IT', 'AA123456789', false],
            // Lithuania
            ['LT', '210061371310', true],
            ['LT', '213179412', true],
            ['LT', '290061371314', true],
            ['LT', '208640716', true],
            ['LT', '21317941', false],
            ['LT', '1234567890', false],
            ['LT', '1234567890AB', false],
            // Luxembourg
            ['LU', '10000356', true],
            ['LU', '1234567', false],
            // Latvia
            ['LV', '40003009497', true],
            ['LV', '1234567890', false],
            // Malta
            ['MT', '15121333', true],
            ['MT', '1234567', false],
            // Netherlands
            ['NL', '010000446B01', true],
            ['NL', '12345678901', false],
            ['NL', '123456789A12', false],
            // Poland
            ['PL', '5260001246', true],
            ['PL', '12342678090', false],
            // Portugal
            ['PT', '502757191', true],
            ['PT', '12345678', false],
            // Romania
            ['RO', '11198699', true],
            ['RO', '14186770', true],
            ['RO', '1', false],
            ['RO', '12345678902', false],
            // Sweden
            ['SE', '556188840401', true],
            ['SE', '1234567890', false],
            // Slovenia
            ['SI', '15012557', true],
            ['SI', '1234567', false],
            ['SI', '957965503', false],
            // Slovakia
            ['SK', '4030000007', true],
            ['SK', '123456789', false],
            ['SK', '01234567890', false],
        ];
    }
}
