<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Tests\Client;

use PHPUnit\Framework\TestCase;
use Semaio\ViesValidation\Client\HeartbeatClient;

/**
 * Class HeartbeatClientTest
 *
 * @package Semaio\ViesValidation\Tests\Client
 */
class HeartbeatClientTest extends TestCase
{
    /**
     * @test
     * @covers \Semaio\ViesValidation\Client\HeartbeatClient::isAlive
     */
    public function isAlive()
    {
        $client = $this->getClient(true);
        $this->assertTrue($client->isAlive());
    }

    /**
     * @test
     * @covers \Semaio\ViesValidation\Client\HeartbeatClient::isAlive
     */
    public function isDead()
    {
        $client = $this->getClient(false);
        $this->assertFalse($client->isAlive());
    }

    private function getClient($result)
    {
        $heartBeatMock = $this->createPartialMock(HeartbeatClient::class, ['isAlive']);

        $heartBeatMock->expects($this->once())
            ->method('isAlive')
            ->will($this->returnValue($result));

        return $heartBeatMock;
    }
}
