<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Tests\Client;

use PHPUnit\Framework\TestCase;
use Semaio\ViesValidation\Client\ViesClient;
use Semaio\ViesValidation\Exception\InvalidCountryCodeException;
use Semaio\ViesValidation\Exception\ViesServiceException;
use Semaio\ViesValidation\Tests\SoapClientMock;
use Semaio\ViesValidation\Validation\SyntaxValidator;
use Semaio\ViesValidation\ViesResponse;
use SoapFault;
use stdClass;

/**
 * Class ViesClientTest
 *
 * @package Semaio\ViesValidation\Tests\Client
 */
class ViesClientTest extends TestCase
{
    /**
     * @test
     */
    public function execute()
    {
        $response = new stdClass();
        $response->countryCode = 'DE';
        $response->vatNumber = '123456789';
        $response->requestDate = '2019-12-05+01:00';
        $response->valid = true;

        $client = $this->getClient(['checkVatApprox' => $response]);

        $response = $client->execute('DE', '123456789');
        $this->assertInstanceOf(ViesResponse::class, $response);
        $this->assertEquals('DE', $response->getCountryCode());
        $this->assertEquals('123456789', $response->getVatNumber());
        $this->assertTrue($response->isValid());
    }

    /**
     * @test
     */
    public function executeWithInvalidSyntax()
    {
        $client = $this->getClient();

        $response = $client->execute('DE', '12345678');
        $this->assertInstanceOf(ViesResponse::class, $response);
        $this->assertEquals('DE', $response->getCountryCode());
        $this->assertEquals('12345678', $response->getVatNumber());
        $this->assertFalse($response->isValid());
    }

    /**
     * @test
     */
    public function executeWithViesFault()
    {
        $this->expectException(ViesServiceException::class);

        $stub = $this->getMockFromWsdl(__DIR__ . '/_fixtures/checkVatTestService.wsdl');
        $stub->expects($this->any())
            ->method('__soapCall')
            ->will($this->throwException(new SoapFault("test", "error message")));

        $client = new ViesClient($stub, new SyntaxValidator());
        $client->execute('DE', '123456789');
    }

    /**
     * @test
     */
    public function executeWithInvalidCountryCode()
    {
        $this->expectException(InvalidCountryCodeException::class);
        $this->expectExceptionMessage('Invalid country code "XY" provided.');

        $client = $this->getClient();
        $client->execute('XY', '123456789');
    }

    /**
     * @test
     */
    public function buildRequestParamsWithInvalidRequestParam()
    {
        $client = $this->getClient();
        $this->assertEquals(
            [
                'countryCode'          => 'DE',
                'vatNumber'            => '123456789',
                'requesterCountryCode' => 'DE',
                'requesterVatNumber'   => '987654321',
                'traderName'           => 'PhpUnit Test Company',
            ],
            $client->buildRequestParams(
                'DE',
                '123456789',
                [
                    'requesterCountryCode' => 'DE',
                    'requesterVatNumber'   => '987654321',
                    'foo'                  => 'bar',
                    'traderName'           => 'PhpUnit "Test" Company',
                    'traderCompanyType'    => '<script>alert("hier");</script>',
                ]
            )
        );
    }

    /**
     * @test
     */
    public function buildRequestParamsWithInvalidRequesterCountryCode()
    {
        $this->expectException(InvalidCountryCodeException::class);
        $this->expectExceptionMessage('Invalid country code "YX" provided.');

        $client = $this->getClient();
        $client->buildRequestParams(
            'DE',
            '123456789',
            [
                'requesterCountryCode' => 'YX',
            ]
        );
    }


    /**
     * @test
     */
    public function normalizeVatNumber()
    {
        $client = $this->getClient();

        $this->assertEquals('123456789', $client->normalizeVatNumber('123456789'));
        $this->assertEquals('123456789', $client->normalizeVatNumber('123 456 789'));
        $this->assertEquals('123456789', $client->normalizeVatNumber('123.456.789'));
        $this->assertEquals('123456789', $client->normalizeVatNumber('123-456-789'));
    }

    private function getClient(array $dummyResponses = [])
    {
        $soapClient = new SoapClientMock('http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl');
        if (count($dummyResponses)) {
            $soapClient->setDummyResponses($dummyResponses);
        }

        $validator = new SyntaxValidator();

        return new ViesClient($soapClient, $validator);
    }
}
