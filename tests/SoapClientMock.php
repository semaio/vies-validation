<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Tests;

/**
 * Class SoapClientMock
 *
 * This class is used to overwrite the functionality of the SoapClient.
 * It is very useful when mocking expected objects from SOAP calls to web services during unit testing.
 *
 * @package Semaio\ViesValidation\Tests
 * @see https://github.com/degaray/mock-soapclient
 */
class SoapClientMock extends \SoapClient
{
    /**
     * @var array
     */
    protected $dummyResponses;

    /**
     * @var mixed
     */
    protected $lastRequest;

    /**
     * Mock __call and retrieve dummy response for call method
     *
     * @param string $name
     * @param array  $arguments
     * @return mixed|null
     */
    public function __call($name, $arguments)
    {
        if (isset($this->dummyResponses[$name])) {
            return $this->dummyResponses[$name];
        }

        return null;
    }

    /**
     * SoapClientMock constructor.
     *
     * @param string $fakeUrl
     * @param array  $fakeParams
     */
    public function __construct($fakeUrl, array $fakeParams = array())
    {
        $this->fakeUrl = $fakeUrl;
        $this->fakeParams = $fakeParams;
    }

    /**
     * @param string $function_name
     * @param array  $arguments
     * @param null   $options
     * @param null   $input_headers
     * @param null   $output_headers
     * @return mixed
     */
    public function __soapCall($function_name, $arguments, $options = null, $input_headers = null, &$output_headers = null)
    {
        return $this->dummyResponses[$function_name];
    }

    /**
     * @param array $dummyResponses
     */
    public function setDummyResponses(array $dummyResponses)
    {
        $this->dummyResponses = $dummyResponses;
    }

    /**
     * @param string $function_name
     * @param mixed  $dummyResponse
     */
    public function setDummyResponse($function_name, $dummyResponse)
    {
        $this->dummyResponses[$function_name] = $dummyResponse;
    }

    /**
     * @param $lastRequest
     */
    public function setLastRequest($lastRequest)
    {
        $this->lastRequest = $lastRequest;
    }

    /**
     * @return mixed|string|null
     */
    public function __getLastRequest()
    {
        if (isset($this->lastRequest)) {
            return $this->lastRequest;
        }

        return null;
    }
}
