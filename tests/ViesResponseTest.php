<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Tests;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Semaio\ViesValidation\ViesResponse;
use stdClass;

/**
 * Class ViesResponseTest
 *
 * @package Semaio\ViesValidation\Tests
 */
class ViesResponseTest extends TestCase
{
    /**
     * @test
     */
    public function create()
    {
        $response = new ViesResponse([
            'countryCode' => 'DE',
            'vatNumber'   => '123456789',
            'requestDate' => '2019-12-05+01:00',
            'valid'       => false,
        ]);

        $this->assertEquals([
            'countryCode'            => 'DE',
            'vatNumber'              => '123456789',
            'requestDate'            => '2019-12-05',
            'valid'                  => false,
            'requestIdentifier'      => '',
            'traderName'             => '---',
            'traderAddress'          => '---',
            'traderNameMatch'        => '',
            'traderCompanyTypeMatch' => '',
            'traderStreetMatch'      => '',
            'traderPostcodeMatch'    => '',
            'traderCityMatch'        => '',
        ], $response->toArray());
    }

    /**
     * @test
     */
    public function createWithMissingRequiredFields()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Required field "requestDate" is missing.');

        $responseData = new stdClass();
        $responseData->countryCode = 'DE';
        $responseData->vatNumber = '123456789';
        $responseData->valid = false;

        new ViesResponse($responseData);
    }
}
