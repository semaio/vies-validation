<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Client;

use Semaio\ViesValidation\Exception\InvalidCountryCodeException;
use Semaio\ViesValidation\Exception\ViesServiceException;
use Semaio\ViesValidation\Validation\SyntaxValidatorInterface;
use Semaio\ViesValidation\ViesResponse;
use SoapClient;
use SoapFault;

/**
 * Class ViesClient
 *
 * @package Semaio\ViesValidation\Client
 */
class ViesClient implements ViesClientInterface
{
    /**
     * @var SoapClient
     */
    private $soapClient;

    /**
     * @var SyntaxValidatorInterface
     */
    private $validator;

    /**
     * ViesClient constructor.
     *
     * @param SoapClient               $soapClient
     * @param SyntaxValidatorInterface $validator
     */
    public function __construct(SoapClient $soapClient, SyntaxValidatorInterface $validator)
    {
        $this->soapClient = $soapClient;
        $this->validator = $validator;
    }

    /**
     * @inheritDoc
     */
    public function execute(string $countryCode, string $vatNumber, array $optionalRequestArguments = []): ViesResponse
    {
        if (!$this->validator->validateCountryCode($countryCode)) {
            throw new InvalidCountryCodeException($countryCode);
        }

        $vatNumber = $this->normalizeVatNumber($vatNumber);

        // Validate
        if (!$this->validator->validate($countryCode, $vatNumber)) {
            return new ViesResponse([
                'countryCode' => $countryCode,
                'vatNumber'   => $vatNumber,
                'requestDate' => date_create(),
                'valid'       => false,
            ]);
        }

        $requestParams = $this->buildRequestParams($countryCode, $vatNumber, $optionalRequestArguments);

        try {
            return new ViesResponse($this->soapClient->__soapCall('checkVatApprox', [$requestParams]));
        } catch (SoapFault $e) {
            $message = sprintf(
                'VIES backend service cannot validate the VAT number "%s%s" at this moment. '
                . 'The service responded with the critical error "%s". This is probably a temporary '
                . 'problem. Please try again later.',
                $countryCode,
                $vatNumber,
                $e->getMessage()
            );
            throw new ViesServiceException($message, 0, $e);
        }
    }

    /**
     * @param string $countryCode
     * @param string $vatNumber
     * @param array  $optionalRequestArguments
     * @return array
     * @throws InvalidCountryCodeException
     */
    public function buildRequestParams(string $countryCode, string $vatNumber, array $optionalRequestArguments): array
    {
        $requestParams = ['countryCode' => $countryCode, 'vatNumber' => $vatNumber];

        foreach ($optionalRequestArguments as $requestArgument => $requestArgumentValue) {
            if (!in_array($requestArgument, static::ALLOWED_OPTIONAL_REQUEST_ARGUMENTS)) {
                continue;
            }

            $argumentValue = str_replace(['"', '\''], '', $requestArgumentValue);
            $argumentValue = filter_var($argumentValue, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_HIGH);
            if (false === filter_var($argumentValue, FILTER_VALIDATE_REGEXP, ['options' => [
                    'regexp' => '/^[a-zA-Z0-9\s\.\-,]+$/',
                ]])) {
                continue;
            }

            if ($requestArgument === 'requesterCountryCode') {
                if (!$this->validator->validateCountryCode($argumentValue)) {
                    throw new InvalidCountryCodeException($argumentValue);
                }
            }

            if ($requestArgument === 'requesterVatNumber') {
                $argumentValue = $this->normalizeVatNumber($argumentValue);
            }

            $requestParams[$requestArgument] = $argumentValue;
        }

        return $requestParams;
    }

    /**
     * @param string $vatNumber
     * @return string
     */
    public function normalizeVatNumber(string $vatNumber): string
    {
        return str_replace([' ', '.', '-'], '', $vatNumber);
    }
}
