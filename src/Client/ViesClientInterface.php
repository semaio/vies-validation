<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Client;

use Semaio\ViesValidation\ViesResponse;

/**
 * Interface ViesClientInterface
 *
 * @package Semaio\ViesValidation\Client
 */
interface ViesClientInterface
{
    const ALLOWED_OPTIONAL_REQUEST_ARGUMENTS = [
        'traderName', 'traderCompanyType', 'traderStreet', 'traderPostcode',
        'traderCity', 'requesterCountryCode', 'requesterVatNumber',
    ];

    /**
     * @param string $countryCode
     * @param string $vatNumber
     * @param array  $optionalRequestArguments
     * @return ViesResponse
     */
    public function execute(string $countryCode, string $vatNumber, array $optionalRequestArguments = []): ViesResponse;
}
