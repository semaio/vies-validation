<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Client;

/**
 * Interface HeartbeatClientInterface
 *
 * @package Semaio\ViesValidation\Client
 */
interface HeartbeatClientInterface
{
    /**
     * @return bool
     */
    public function isAlive(): bool;
}
