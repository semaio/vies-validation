<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Client;

/**
 * Class HeartbeatClient
 *
 * @package Semaio\ViesValidation\Client
 */
class HeartbeatClient implements HeartbeatClientInterface
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $port;

    /**
     * HeartbeatClient constructor.
     *
     * @param string $host
     * @param int    $port
     */
    public function __construct(string $host, int $port = 80)
    {
        $this->host = $host;
        $this->port = $port;
    }

    /**
     * @inheritDoc
     * @codeCoverageIgnore
     */
    public function isAlive(): bool
    {
        return false !== fsockopen($this->host, $this->port);
    }
}
