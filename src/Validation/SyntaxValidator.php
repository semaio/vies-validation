<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Validation;

/**
 * Class SyntaxSyntaxValidator
 *
 * @package Semaio\ViesValidation\Validation
 */
class SyntaxValidator implements SyntaxValidatorInterface
{
    /**
     * @var array
     */
    private $patterns = [
        'AT' => 'U[0-9]{8}',
        'BE' => '0[0-9]{9}',
        'BG' => '[0-9]{9,10}',
        'CY' => '[0-9]{8}[A-Z]',
        'CZ' => '[0-9]{8,10}',
        'DE' => '[0-9]{9}',
        'DK' => '[0-9]{8}',
        'EE' => '[0-9]{9}',
        'EL' => '[0-9]{9}',
        'ES' => '[0-9A-Z][0-9]{7}[0-9A-Z]',
        'FI' => '[0-9]{8}',
        'FR' => '[0-9A-Z]{2}[0-9]{9}',
        'GB' => '([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3})',
        'HR' => '[0-9]{11}',
        'HU' => '[0-9]{8}',
        'IE' => '[0-9]{7}[A-Z]{1,2}',
        'IT' => '[0-9]{11}',
        'LT' => '([0-9]{9}|[0-9]{12})',
        'LU' => '[0-9]{8}',
        'LV' => '[0-9]{11}',
        'MT' => '[0-9]{8}',
        'NL' => '[0-9]{9}B[0-9]{2}',
        'PL' => '[0-9]{10}',
        'PT' => '[0-9]{9}',
        'RO' => '[0-9]{2,10}',
        'SE' => '[0-9]{12}',
        'SI' => '[0-9]{8}',
        'SK' => '[0-9]{10}',
    ];

    /**
     * @inheritDoc
     */
    public function validate(string $countryCode, string $vatNumber): bool
    {
        if (!$this->validateCountryCode($countryCode)) {
            return false;
        }

        if (0 === preg_match('/^(?:' . $this->patterns[$countryCode] . ')$/', $vatNumber)) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function validateCountryCode(string $countryCode): bool
    {
        return isset($this->patterns[$countryCode]);
    }
}
