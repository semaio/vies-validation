<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Validation;

/**
 * Interface SyntaxValidatorInterface
 *
 * @package Semaio\ViesValidation\Validation
 */
interface SyntaxValidatorInterface
{
    /**
     * @param string $countryCode
     * @param string $vatNumber
     * @return bool
     */
    public function validate(string $countryCode, string $vatNumber): bool;

    /**
     * @param string $countryCode
     * @return bool
     */
    public function validateCountryCode(string $countryCode): bool;
}
