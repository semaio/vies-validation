<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation;

use Semaio\ViesValidation\Client\HeartbeatClientInterface;
use Semaio\ViesValidation\Client\ViesClientInterface;

/**
 * Interface ViesInterface
 *
 * @package Semaio\ViesValidation
 */
interface ViesInterface
{
    /**
     * @return ViesClientInterface
     */
    public function getViesClient(): ViesClientInterface;

    /**
     * @return HeartbeatClientInterface
     */
    public function getHeartbeatClient(): HeartbeatClientInterface;
}
