<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation;

use Semaio\ViesValidation\Client\HeartbeatClient;
use Semaio\ViesValidation\Client\ViesClient;
use Semaio\ViesValidation\Validation\SyntaxValidator;
use Semaio\ViesValidation\Validation\SyntaxValidatorInterface;
use SoapClient;
use SoapFault;

/**
 * Class ViesBuilder
 *
 * @package Semaio\ViesValidation
 */
class ViesBuilder
{
    const VIES_PROTOCOL = 'http';
    const VIES_DOMAIN = 'ec.europa.eu';
    const VIES_WSDL = '/taxation_customs/vies/checkVatService.wsdl';

    /**
     * @var SoapClient|null
     */
    private $soapClient = null;

    /**
     * @var string|null
     */
    private $wsdl = null;

    /**
     * @var array|null
     */
    private $soapOptions = null;

    /**
     * @var SyntaxValidatorInterface|null
     */
    private $validator = null;

    /**
     * @return Vies
     * @throws SoapFault
     */
    public function build(): Vies
    {
        $viesClient = new ViesClient($this->getSoapClient(), $this->getValidator());
        $heartbeatClient = new HeartbeatClient('tcp://' . static::VIES_DOMAIN);

        return new Vies($viesClient, $heartbeatClient);
    }

    /**
     * @return SoapClient
     * @throws SoapFault
     */
    public function getSoapClient(): SoapClient
    {
        return $this->soapClient ?? new SoapClient($this->getWsdl(), $this->getSoapOptions());
    }

    /**
     * @param SoapClient $soapClient
     * @return $this
     */
    public function setSoapClient(SoapClient $soapClient): self
    {
        $this->soapClient = $soapClient;

        return $this;
    }

    /**
     * @return string
     */
    public function getWsdl(): string
    {
        return $this->wsdl ?? sprintf('%s://%s%s', static::VIES_PROTOCOL, static::VIES_DOMAIN, static::VIES_WSDL);
    }

    /**
     * @param string $wsdl
     * @return $this
     */
    public function setWsdl(string $wsdl): self
    {
        $this->wsdl = $wsdl;

        return $this;
    }

    /**
     * @return array
     */
    public function getSoapOptions(): array
    {
        return $this->soapOptions ?? [];
    }

    /**
     * @param array $soapOptions
     * @return $this
     */
    public function setSoapOptions(array $soapOptions): self
    {
        $this->soapOptions = $soapOptions;

        return $this;
    }

    /**
     * @return SyntaxValidatorInterface
     */
    public function getValidator(): SyntaxValidatorInterface
    {
        return $this->validator ?? new SyntaxValidator();
    }

    /**
     * @param SyntaxValidatorInterface $validator
     * @return ViesBuilder
     */
    public function setValidator(SyntaxValidatorInterface $validator): self
    {
        $this->validator = $validator;

        return $this;
    }
}
