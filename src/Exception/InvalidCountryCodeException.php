<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation\Exception;

use Exception;
use Throwable;

/**
 * Class InvalidCountryCodeException
 *
 * @package Semaio\ViesValidation\Exception
 */
class InvalidCountryCodeException extends Exception
{
    /**
     * @inheritDoc
     */
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        $message = sprintf('Invalid country code "%s" provided.', $message);

        parent::__construct($message, $code, $previous);
    }
}
