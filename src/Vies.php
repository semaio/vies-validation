<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation;

use Semaio\ViesValidation\Client\HeartbeatClientInterface;
use Semaio\ViesValidation\Client\ViesClientInterface;

/**
 * Class Vies
 *
 * @package Semaio\ViesValidation
 */
class Vies implements ViesInterface
{
    /**
     * @var ViesClientInterface
     */
    private $viesClient;

    /**
     * @var HeartbeatClientInterface
     */
    private $heartbeatClient;

    /**
     * Vies constructor.
     *
     * @param ViesClientInterface      $viesClient
     * @param HeartbeatClientInterface $heartbeatClient
     */
    public function __construct(ViesClientInterface $viesClient, HeartbeatClientInterface $heartbeatClient)
    {
        $this->viesClient = $viesClient;
        $this->heartbeatClient = $heartbeatClient;
    }

    /**
     * @inheritDoc
     */
    public function getViesClient(): ViesClientInterface
    {
        return $this->viesClient;
    }

    /**
     * @inheritDoc
     */
    public function getHeartbeatClient(): HeartbeatClientInterface
    {
        return $this->heartbeatClient;
    }
}
