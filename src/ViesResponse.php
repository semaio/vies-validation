<?php
declare(strict_types=1);

/*
 * This file is part of the VIES validation library.
 *
 * (c) semaio GmbH
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Semaio\ViesValidation;

use DateTime;
use InvalidArgumentException;
use stdClass;

/**
 * Class ViesResponse
 *
 * @package Semaio\ViesValidation
 */
class ViesResponse
{
    const VIES_DATETIME_FORMAT = 'Y-m-dP';

    /**
     * @var string
     */
    private $countryCode;

    /**
     * @var string
     */
    private $vatNumber;

    /**
     * @var DateTime
     */
    private $requestDate;

    /**
     * @var bool
     */
    private $valid;

    /**
     * @var string
     */
    private $traderName;

    /**
     * @var string
     */
    private $traderAddress;

    /**
     * @var string
     */
    private $requestIdentifier;

    /**
     * @var string
     */
    private $traderNameMatch;

    /**
     * @var string
     */
    private $traderCompanyTypeMatch;

    /**
     * @var string
     */
    private $traderStreetMatch;

    /**
     * @var string
     */
    private $traderPostcodeMatch;

    /**
     * @var string
     */
    private $traderCityMatch;

    /**
     * @param array|stdClass $row
     */
    public function __construct($row)
    {
        if (is_array($row)) {
            $row = (object)$row;
        }

        $requiredFields = ['countryCode', 'vatNumber', 'requestDate', 'valid'];
        foreach ($requiredFields as $requiredField) {
            if (!isset($row->{$requiredField})) {
                throw new InvalidArgumentException('Required field "' . $requiredField . '" is missing.');
            }
        }

        $requestDate = $row->requestDate;
        if (!$row->requestDate instanceof DateTime) {
            $requestDate = date_create_from_format(static::VIES_DATETIME_FORMAT, $row->requestDate);
            $requestDate->setTime(0, 0, 0, 0);
        }

        // required parameters
        $this->countryCode = $row->countryCode;
        $this->vatNumber = $row->vatNumber;
        $this->requestDate = $requestDate;
        $this->valid = $row->valid;

        $this->requestIdentifier = $row->requestIdentifier ?? '';
        $this->traderName = $row->traderName ?? '---';
        $this->traderAddress = $row->traderAddress ?? '---';
        $this->traderNameMatch = $row->traderNameMatch ?? '';
        $this->traderCompanyTypeMatch = $row->traderCompanyTypeMatch ?? '';
        $this->traderStreetMatch = $row->traderStreetMatch ?? '';
        $this->traderPostcodeMatch = $row->traderPostcodeMatch ?? '';
        $this->traderCityMatch = $row->traderCityMatch ?? '';
    }

    /**
     * @return string
     */
    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    /**
     * @return string
     */
    public function getVatNumber(): string
    {
        return $this->vatNumber;
    }

    /**
     * @return DateTime
     */
    public function getRequestDate(): DateTime
    {
        return $this->requestDate;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->valid;
    }

    /**
     * @return string
     */
    public function getTraderName(): string
    {
        return $this->traderName;
    }

    /**
     * @return string
     */
    public function getTraderAddress(): string
    {
        return $this->traderAddress;
    }

    /**
     * @return string
     */
    public function getRequestIdentifier(): string
    {
        return $this->requestIdentifier;
    }

    /**
     * @return string
     */
    public function getTraderNameMatch(): string
    {
        return $this->traderNameMatch;
    }

    /**
     * @return string
     */
    public function getTraderCompanyTypeMatch(): string
    {
        return $this->traderCompanyTypeMatch;
    }

    /**
     * @return string
     */
    public function getTraderStreetMatch(): string
    {
        return $this->traderStreetMatch;
    }

    /**
     * @return string
     */
    public function getTraderPostcodeMatch(): string
    {
        return $this->traderPostcodeMatch;
    }

    /**
     * @return string
     */
    public function getTraderCityMatch(): string
    {
        return $this->traderCityMatch;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'countryCode'            => $this->getCountryCode(),
            'vatNumber'              => $this->getVatNumber(),
            'requestDate'            => $this->getRequestDate()->format('Y-m-d'),
            'valid'                  => $this->isValid(),
            'requestIdentifier'      => $this->getRequestIdentifier(),
            'traderName'             => $this->getTraderName(),
            'traderAddress'          => $this->getTraderAddress(),
            'traderNameMatch'        => $this->getTraderNameMatch(),
            'traderCompanyTypeMatch' => $this->getTraderCompanyTypeMatch(),
            'traderStreetMatch'      => $this->getTraderStreetMatch(),
            'traderPostcodeMatch'    => $this->getTraderPostcodeMatch(),
            'traderCityMatch'        => $this->getTraderCityMatch(),
        ];
    }
}
